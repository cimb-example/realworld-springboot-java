FROM openjdk:11

ENV JAVA_VERSION=11.0.10

ENV LANG=C.UTF-8

ENV PATH=/usr/local/openjdk-11/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

ENV JAVA_HOME=/usr/local/openjdk-11

WORKDIR /usr/src/myapp

COPY ./gradle/ ./gradle
COPY ./src/ ./src

COPY build.gradle build.gradle
COPY settings.gradle settings.gradle
COPY test.gradle test.gradle
COPY gradlew gradlew

# RUN ./gradlew dependencyCheckAnalyze

# RUN ./gradlew test

# RUN ./gradlew test jacocoTestReport

# RUN ./gradlew jacocoTestReport

RUN ./gradlew build

EXPOSE 8080

CMD ["./gradlew", "bootRun"]