// Define variables
def scmVars

// Start Pipeline
pipeline {

  // Configure Jenkins Slave
  agent {
    // Use Kubernetes as dynamic Jenkins Slave
    kubernetes {
      // Kubernetes Manifest File to spin up Pod to do build
      yaml """
apiVersion: v1
kind: Pod
spec:
  containers:
  - name: java
    image: openjdk:11
    command:
    - cat
    tty: true   
    volumeMounts:
    - name: dependency-check-data
      mountPath: /home/jenkins/.gradle/dependency-check-data
  - name: k8s
    image: alpine/k8s:1.23.15
    command:
    - cat
    tty: true
  - name: kaniko
    image: gcr.io/kaniko-project/executor:v1.9.1-debug
    imagePullPolicy: Always
    command:
    - sleep
    args:
    - 99d
    volumeMounts:
    - name: kaniko-secret
      mountPath: /kaniko/.docker
      readOnly: true
  - name: crane
    image: gcr.io/go-containerregistry/crane:debug
    imagePullPolicy: Always
    entrypoint: sh
    command:
    - sleep
    args:
    - 99d
    volumeMounts:
    - name: kaniko-secret
      mountPath: /root/.docker
      readOnly: true
  volumes:
  - name: kaniko-secret
    secret:
      secretName: kaniko-secret
  - name: dependency-check-data
    hostPath:
      path: /tmp/dependency-check-data
"""
    } // End kubernetes
  } // End agent
    environment {
    BRANCH_NAME = 'master'
  
        // The following environment variables will be difference depend on project.
      PROJECT_REPOSITORY_SSH = 'git@git.opsta.io:cimbth/terraform-cci-project/demo-springboot-cicd.git'
      ARGOCD_REPOSITORY_SSH = 'git@git.opsta.io:cimbth/terraform-cci-project/demo-argocd-repository.git'
      ARGOCD_REPOSITORY_URL = 'git.opsta.io/cimbth/terraform-cci-project/demo-argocd-repository.git'
      IMAGE_REGISTRY = '967672538577.dkr.ecr.ap-southeast-1.amazonaws.com/cimb-example-springboot'
      KUSTOMIZE_FILE_PATH = "${WORKSPACE}/config/example/springboot/overlays/${ENV}/deployment.yaml"
  }

  // Start Pipeline
  stages {

    // ***** Stage Clone *****
    stage('Clone ratings source code') {

      // Steps to run build
      steps {
        // Run in Jenkins Slave container
        container('jnlp') {
          // Use script to run
          script {
            // Git clone repo and checkout branch as we put in parameter
            scmVars = git branch: "${BRANCH_NAME}",
                          credentialsId: 'jj-test-key',
                          url: "${PROJECT_REPOSITORY_SSH}"
          } // End script
        } // End container
      } // End steps
    } // End stage

    // ***** Stage Unit-test *****
    stage('Unit Test') {
        when {
          // expression { return env.BRANCH_NAME.contains('dev') }
          expression { return env.ENV.contains('dev') }
        }
        steps {
          container('java'){
            script {
              // Java Gradle dependencies check
              sh '''./gradlew test jacocoTestReport'''
            } // End script
          } // End container
        } // End steps
    } // End stage

    // ***** Stage Sonarqube *****
    // stage('Sonarqube Scanner') {
    //     when {
    //       // expression { return env.BRANCH_NAME.contains('dev') }
    //       expression { return env.ENV.contains('dev') }
    //     }
    //     environment {
    //       SCANNER_HOME = tool 'sonarqube-scanner' // Install tool sonarqube on Jenkins
    //       PROJECT_KEY = "cimb-example-springboot"
    //       PROJECT_NAME = "cimb-example-springboot"
    //       PROJECT_VERSION = "${scmVars.GIT_COMMIT}"
    //      } 
    //     steps {
    //       container('java'){
    //         script {
    //           // Authentiocation with https://sonarqube.infra.jenosize.io
    //           withSonarQubeEnv('sonarqube-scanner') {
    //               // Run Sonar Scanner
    //               sh '''${SCANNER_HOME}/bin/sonar-scanner \
    //               -D sonar.projectKey=${PROJECT_KEY} \
    //               -D sonar.projectName=${PROJECT_NAME} \
    //               -D sonar.projectVersion=${PROJECT_VERSION} \
    //               -D sonar.sources=./src
    //               -D sonar.go.coverage.reportPaths=$CI_PROJECT_DIR/coverage.out
    //               '''
    //           }//End withSonarQubeEnv

    //           // Run Quality Gate
    //           timeout(time: 1, unit: 'MINUTES') { 
    //               def qg = waitForQualityGate(webhookSecretId: 'sonarqube_secret')
    //               if (qg.status != 'OK') {
    //                   error "Pipeline aborted due to quality gate failure: ${qg.status}"
    //               }
    //           } // End Timeout
    //         } // End script
    //       } // End container
    //     } // End steps
    // } // End stage

    // ***** Stage OWASP *****
    stage('OWASP Dependency Check') {
      when {
        // expression { return env.BRANCH_NAME.contains('dev') } 
        expression { return env.ENV.contains('dev') }
      }
      steps {
        container('java') {
          script {
            // Java Gradle dependencies check
            sh '''./gradlew dependencyCheckAnalyze'''

            // Publish report to Jenkins
            dependencyCheckPublisher(
              pattern: 'build/reports/dependency-check-report.html'
            )
          } // End script
        } // End container
      } // End steps
    } // End stage

    // ***** Stage Build *****
    stage('Build Docker Image and push') {
      when { 
        expression { return env.ENV.contains('dev') }
      }
      environment {
        IMAGE_TAG = "${scmVars.GIT_COMMIT}"
      }
      steps {
        container('kaniko') {
          script {
            sh "echo ${IMAGE_TAG}"
            sh "/kaniko/executor --context $WORKSPACE --dockerfile $WORKSPACE/Dockerfile --verbosity debug --destination ${IMAGE_REGISTRY}:${IMAGE_TAG}"
          } // End script
        } // End container
      } // End steps
    } // End stage

    // ***** Stage Re-tag image *****
    stage('Retag Image for deploy production') {
      when {
          expression { return env.ENV.contains('prd') }
      }
      environment {
        IMAGE_TAG = "${scmVars.GIT_COMMIT}"
      }
      steps {
        // Run on Helm container
        container('crane') {
          script {
            // Use kubeconfig from Jenkins Credential
            sh "crane cp ${IMAGE_REGISTRY}:${IMAGE_TAG} ${IMAGE_REGISTRY}:${tags}"
          } // End script
        } // End container
      } // End steps
    } // End stage

    // ***** Stage Deploy *****
    stage('Deploy') {
      // when { 
      //   not {
      //     expression { return env.ENV.contains('prd') }
      //   }
      // }
      environment {
        IMAGE_TAG = "${scmVars.GIT_COMMIT}"
      }
      steps {
        // Run on Helm container
        container('k8s') {
          script {
            // Use kubeconfig from Jenkins Credential
            scmVars = git branch: "main",
                          credentialsId: 'jj-test-key',
                          url: "${ARGOCD_REPOSITORY_SSH}"
            if (env.ENV.contains('prd')) {
              sh "yq -i \".spec.template.spec.containers[0].image = \\\"${IMAGE_REGISTRY}:${tags}\\\"\" ${KUSTOMIZE_FILE_PATH}"
            } else {
              sh "yq -i \".spec.template.spec.containers[0].image = \\\"${IMAGE_REGISTRY}:${IMAGE_TAG}\\\"\" ${KUSTOMIZE_FILE_PATH}"
            }
            sh "cat ${KUSTOMIZE_FILE_PATH}"
            sh "git config --global --add safe.directory ${WORKSPACE}"
            sh "git config --global user.email \"jenkins@opsta.co.th\""
            sh "git config --global user.name \"Jenkins\""
            sh "git config --global --add safe.directory ${WORKSPACE}"
            sh "git add ${KUSTOMIZE_FILE_PATH}"
            sh "git commit -m \"Edit vaule in file ${KUSTOMIZE_FILE_PATH}\""
            withCredentials([string(credentialsId: 'gitlab-access-token', variable: 'GIT_ACCESS_TOKEN')]) { // Setup gitlab-access-token in environemnt variable
              sh "git push https://gitlab-ci-token:${GIT_ACCESS_TOKEN}@${ARGOCD_REPOSITORY_URL} main"
            }
          } // End script
        } // End container
      } // End steps
    } // End stage

  } // End stages
} // End pipeline